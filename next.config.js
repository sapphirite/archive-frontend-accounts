const path = require("path");
const dotEnvResult = require("dotenv").config();
const withTypescript = require("@zeit/next-typescript");

module.exports = withTypescript({
    publicRuntimeConfig: {
        openidUrl: process.env.OPENID_URL,
        clientApiAccountsUrl: process.env.CLIENT_API_ACCOUNTS_URL,
        serverApiAccountsUrl: process.env.SERVER_API_ACCOUNTS_URL,
    },
    webpack: config => {
        // Add the ~/ root path for typescript imports
        config.resolve.alias = {
            ...config.resolve.alias,
            "~": path.resolve(__dirname, "."),
        }
        return config
    }
});
