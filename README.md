# Sapphirite - Frontend Accounts

## Environment Variables
- OPENID_URL:
    An URL pointing to the OpenID Connect server to use for logging in.
    The oauth2 routes are discovered through "/.well-known/openid-configuration".
    For example: "http://example.com/"
- CLIENT_API_ACCOUNTS_URL:
    An URL pointing to the Sapphirite Accounts API as accessed by the client.
    For example: "https://example.com/api/accounts"
- SERVER_API_ACCOUNTS_URL:
    An URL pointing to the Sapphirite Accounts API as accessed by the server.
    For example: "https://api-accounts/api"

## Development Setup
1. Copy the example "/.env.example" file to "/.env".
2. Follow the "Development Setup" instructions for "api-accounts".

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev
```

## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
