FROM node:11-alpine

WORKDIR /app

# Install dependencies
COPY package.json yarn.lock ./
RUN yarn install

# Copy source files
COPY . .

# Build the app
RUN yarn run build

CMD [ "npm", "start" ]