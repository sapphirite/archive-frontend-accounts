import { Paper, Typography } from "@material-ui/core";
import { Theme, createStyles, WithStyles, withStyles } from "@material-ui/core/styles";
import ErrorIcon from "@material-ui/icons/Error";

const styles = (theme: Theme) => createStyles({
    error: {
        backgroundColor: theme.palette.error.dark,
        color: theme.palette.error.contrastText,

        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing.unit * 2,
    },
    errorText: {
        color: theme.palette.error.contrastText,
        marginLeft: theme.spacing.unit * 1,
    }
});

interface Props extends WithStyles<typeof styles> {
    error: string,
}

const FormError: React.FunctionComponent<Props> = ({ classes, error = "" }) => {
    return (
        <Paper className={classes.error}>
            <ErrorIcon />
            <Typography component="span" variant="body1" className={classes.errorText}>
                {error}
            </Typography>
        </Paper>
    )
};

export default withStyles(styles)(FormError);
