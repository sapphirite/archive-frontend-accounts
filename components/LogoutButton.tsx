import Router from "next/router";
import { Button } from "@material-ui/core";
import { UserManager } from "oidc-client";

import getUserManager from "~/src/getUserManager";

const LogoutButton: React.FunctionComponent = _props => {
    async function logoutClick() {
        const userManager = getUserManager() as UserManager;
        await userManager.removeUser();
        Router.push({
            pathname: "/auth/logout-redirect",
            query: { redirect_uri: window.location.origin }
        });
    }

    return <Button variant="contained" color="primary" onClick={logoutClick}>Log Out</Button>;
};

export default LogoutButton;