import { Button } from "@material-ui/core";
import { UserManager } from "oidc-client";
import getUserManager from "~/src/getUserManager";

const LoginButton: React.FunctionComponent = _props => {
    async function loginClick() {
        const userManager = getUserManager() as UserManager;
        await userManager.signinRedirect();
    }

    return <Button variant="contained" color="primary" onClick={loginClick}>Log In</Button>;
};

export default LoginButton;