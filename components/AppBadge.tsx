import { Card, CardContent, Typography, CardMedia } from "@material-ui/core";
import { Theme, createStyles, WithStyles, withStyles } from "@material-ui/core/styles";

const styles = (theme: Theme) => createStyles({
    card: {
        display: "flex",
    },
    content: {
        flex: "1 0 auto",
    },
    logo: {
        width: 75,
        height: 75,
        margin: theme.spacing.unit,
    }
});

interface Props extends WithStyles<typeof styles> {
    clientName: string,
    clientOwner: string,
}

const AppBadge: React.FunctionComponent<Props> = ({ classes, clientName, clientOwner }) => {
    return (
        <Card className={classes.card}>
            <CardContent className={classes.content}>
                <Typography component="h5" variant="h5">
                    {clientName}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                    By: {clientOwner}
                </Typography>
            </CardContent>
            <CardMedia
                image="/static/logomark-circle.png"
                title={clientName}
                className={classes.logo}
            />
        </Card>
    )
};

export default withStyles(styles)(AppBadge);
