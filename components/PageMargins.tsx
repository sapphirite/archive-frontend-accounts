import { withStyles, Theme, createStyles, WithStyles } from "@material-ui/core/styles";

const styles = (theme: Theme) => createStyles({
    layout: {
        width: "auto",
        margin: theme.spacing.unit * 3,
    },
    layoutBig: {
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: "auto",
            marginRight: "auto",
        },
    },
    layoutSmall: {
        [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
            width: 600,
            marginLeft: "auto",
            marginRight: "auto",
        },
    }
});

interface Props extends WithStyles<typeof styles> {
    small?: boolean,
}

const PageMargins: React.FunctionComponent<Props> = ({ children, classes, small = false }) => {
    let classNames =
        classes.layout + " " +
        (small ? classes.layoutSmall : classes.layoutBig);
    return (
        <div className={classNames}>
            {children}
        </div>
    )
};

export default withStyles(styles)(PageMargins);
