import React, { SyntheticEvent } from "react";
import Router from "next/router";
import { Grid, Button, FormControl, InputLabel, Input, FormControlLabel, Checkbox, LinearProgress } from "@material-ui/core";
import { Theme, createStyles, WithStyles, withStyles } from "@material-ui/core/styles";

import AccountsService, { isApiAccountsError } from "~/src/AccountsService";
import FormError from "~/components/FormError";
import AuthPaper from "~/components/auth/AuthPaper";

const styles = (theme: Theme) => createStyles({
    buttons: {
        marginTop: theme.spacing.unit * 2,
    },
    progress: {
        marginTop: theme.spacing.unit * 2,
    }
});

interface Props extends WithStyles<typeof styles> {
    challenge: string,
}

interface State {
    loading: boolean,
    error: string | null,

    email: string,
    password: string,
    remember: boolean,
}

class LoginPage extends React.Component<Props, State> {
    accounts = new AccountsService();

    constructor(props: Props) {
        super(props);

        this.state = {
            loading: false,
            error: null,

            email: "",
            password: "",
            remember: false,
        };

        this.loginSubmit = this.loginSubmit.bind(this);
    }

    async loginSubmit(e: SyntheticEvent) {
        e.preventDefault();

        // If we're already doing something, don't allow a login attempt
        if (this.state.loading) {
            return
        }
        
        // Lock the UI
        this.setState({loading: true, error: null});

        // Attempt to log in
        const challenge = this.props.challenge;
        const result = await this.accounts.putLoginRequestAttempt(
            challenge,
            {
                email: this.state.email,
                password: this.state.password,
                remember: this.state.remember,
            }
        );

        // Check the result
        if (!isApiAccountsError(result)) {
            Router.push(result.redirectTo);
        } else {
            this.setState({error: result.error, loading: false});
        }
    }

    render() {
        const classes = this.props.classes;

        return (
            <AuthPaper>
                {this.state.error && <FormError error={this.state.error}/>}
                <form onSubmitCapture={this.loginSubmit}>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">Email Address</InputLabel>
                        <Input
                            onChange={e => this.setState({email: e.target.value})}
                            id="email" type="email" autoFocus
                        />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Password</InputLabel>
                        <Input
                            onChange={e => this.setState({password: e.target.value})}
                            id="password" type="password"
                        />
                    </FormControl>
                    <FormControlLabel
                        control={<Checkbox
                            onChange={e => this.setState({remember: e.target.checked})}
                            color="primary"
                        />}
                        label="Remember me"
                    />
                    <Grid
                        container
                        justify="flex-end"
                        spacing={16}
                        className={classes.buttons}
                    >
                        <Grid item>
                            <Button variant="text" disabled>
                                Create a New Account
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button
                                variant="contained"
                                color="primary"
                                type="submit"
                                disabled={this.state.loading}
                            >
                                Login
                            </Button>
                        </Grid>
                    </Grid>
                    {this.state.loading && <LinearProgress className={classes.progress} />}
                </form>
            </AuthPaper>
        )
    }
}

export default withStyles(styles)(LoginPage);