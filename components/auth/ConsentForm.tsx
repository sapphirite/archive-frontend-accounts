import React from "react";
import Router from "next/router";

import { Typography, List, ListItem, ListItemAvatar, Avatar, ListItemText, Grid, Button, LinearProgress } from "@material-ui/core";
import { red, green } from "@material-ui/core/colors";
import { Theme, createStyles, WithStyles, withStyles } from "@material-ui/core/styles";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

import AccountsService from "~/src/AccountsService";
import AuthPaper from "~/components/auth/AuthPaper";

function scopeToFriendly(scope: string): string {
    switch (scope) {
        case "openid":
            return "know which account you are using.";
        default:
            return `use the '${scope}' scope with your account.`;
    }
}

const styles = (theme: Theme) => createStyles({
    buttons: {
        marginTop: theme.spacing.unit * 2,
    },
    buttonReject: {
        backgroundColor: red[500],
        '&:hover': {
            backgroundColor: red[700],
        }
    },
    buttonAccept: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        }
    },
    progress: {
        marginTop: theme.spacing.unit * 2,
    }
});

interface Props extends WithStyles<typeof styles> {
    challenge: string,
    requestedScope: string[],
}

interface State {
    loading: boolean,
}

class ConsentForm extends React.Component<Props, State> {
    accounts = new AccountsService();

    constructor(props: Props) {
        super(props);
        
        this.state = {
            loading: false,
        };
        
        this.accept = this.accept.bind(this);
        this.reject = this.reject.bind(this);
    }

    async accept() {
        this.setState({loading: true});

        const result = await this.accounts.putConsentRequestAccept(this.props.challenge);
        Router.push(result.redirectTo as string);
    }

    async reject() {
        this.setState({loading: true});

        const result = await this.accounts.putConsentRequestReject(this.props.challenge);
        Router.push(result.redirectTo as string);
    }

    render() {
        const classes = this.props.classes;

        return (
            <AuthPaper>
                <Typography component="h1" variant="h5">
                    This app wants to...
                </Typography>
                <List>
                    {this.props.requestedScope.map(scope =>
                        <ListItem key={scope}>
                            <ListItemAvatar>
                                <Avatar>
                                    <AccountCircleIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={"... " + scopeToFriendly(scope)} />
                            {/*<ListItemSecondaryAction>
                                <IconButton>
                                    <InfoIcon />
                                </IconButton>
                            </ListItemSecondaryAction>*/}
                        </ListItem>
                    )}
                </List>
                <Grid
                    container
                    justify="flex-end"
                    spacing={16}
                    className={classes.buttons}
                >
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            disabled={this.state.loading}
                            className={classes.buttonReject}
                            onClick={this.reject}
                        >
                            Reject
                        </Button>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.buttonAccept}
                            disabled={this.state.loading}
                            onClick={this.accept}
                        >
                            Accept
                        </Button>
                    </Grid>
                </Grid>
                {this.state.loading && <LinearProgress className={classes.progress} />}
            </AuthPaper>
        )
    }
}

export default withStyles(styles)(ConsentForm);