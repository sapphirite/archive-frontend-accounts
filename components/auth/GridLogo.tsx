import { Grid } from "@material-ui/core";
import { withStyles, Theme, createStyles, WithStyles } from "@material-ui/core/styles";

const styles = (_theme: Theme) => createStyles({
    center: {
        textAlign: "center",
    }
});

interface Props extends WithStyles<typeof styles> {
}

const GridLogo: React.FunctionComponent<Props> = ({ classes }) => {
    return (
        <Grid item xs={12} className={classes.center}>
            <img src="/static/logotype-transparent.png" height="100" />
        </Grid>
    )
}

export default withStyles(styles)(GridLogo);