import { Grid, MuiThemeProvider, createMuiTheme } from "@material-ui/core";

import GridLogo from "~/components/auth/GridLogo";
import PageMargins from "~/components/PageMargins";

const AuthPage: React.FunctionComponent = ({ children }) => {
    const theme = createMuiTheme({
        palette: { type: "light" },
        typography: { useNextVariants: true },
    });

    return (
        <PageMargins small={true}>
            <MuiThemeProvider theme={theme}>
                <Grid container spacing={16}>
                    <GridLogo/>
                    {children}
                </Grid>
            </MuiThemeProvider>
        </PageMargins>
    )
};

export default AuthPage;
