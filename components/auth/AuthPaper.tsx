import { Paper } from "@material-ui/core";
import { Theme, createStyles, WithStyles, withStyles } from "@material-ui/core/styles";

const styles = (theme: Theme) => createStyles({
    paper: {
        padding: theme.spacing.unit * 2,
    },
});

interface Props extends WithStyles<typeof styles> {
}

const LogoutPage: React.FunctionComponent<Props> = ({ classes, children }) => {
    return (
        <Paper className={classes.paper}>
            {children}
        </Paper>
    )
};

export default withStyles(styles)(LogoutPage);
