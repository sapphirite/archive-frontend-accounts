import React from "react";
import { NextContext } from "next";
import Head from "next/head";
import Router from "next/router";
import { Grid } from "@material-ui/core";

import AuthPage from "~/components/auth/AuthPage";
import AppBadge from "~/components/AppBadge";
import LoginForm from "~/components/auth/LoginForm";
import AccountsService from "~/src/AccountsService";

interface Props {
    challenge: string,
    clientName: string,
    clientOwner: string,
}

class LoginPage extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    static async getInitialProps({query, res}: NextContext): Promise<Partial<Props>> {
        const challenge = query.login_challenge as string;

        const accounts = new AccountsService();
        const result = await accounts.putLoginRequestStart(challenge);

        if (result.redirectTo) {
            if (res) {
                res.writeHead(302, { Location: result.redirectTo });
                res.end();
            } else {
                Router.push(result.redirectTo);
            }
            return {}
        }

        return {
            challenge: challenge,
            clientName: result.clientName,
            clientOwner: result.clientOwner,
        }
    }

    render() {
        return (
            <AuthPage>
                <Head>
                    <title>Sapphirite Accounts | Log In</title>
                </Head>
                <Grid item xs={12}>
                    <AppBadge
                        clientName={this.props.clientName}
                        clientOwner={this.props.clientOwner}
                    />
                </Grid>
                <Grid item xs={12}>
                    <LoginForm challenge={this.props.challenge}/>
                </Grid>
            </AuthPage>
        )
    }
}

export default LoginPage;