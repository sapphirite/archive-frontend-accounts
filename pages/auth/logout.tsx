import React from "react";
import Head from "next/head";
import { Grid, Typography, LinearProgress } from "@material-ui/core";

import AuthPage from "~/components/auth/AuthPage";
import AuthPaper from "~/components/auth/AuthPaper";

interface State {
    doesNotRedirect: boolean,
}

class LogoutPage extends React.Component<{}, State> {
    constructor(props: {}) {
        super(props);

        this.state = { doesNotRedirect: false };
    }

    componentDidMount() {
        // Check if we have a stored redirect uri
        const item = window.localStorage.getItem("sa-accounts.logout-redirect");
        if (item) {
            window.localStorage.removeItem("sa-accounts.logout-redirect");
            window.location.href = item;
        } else {
            this.setState({ doesNotRedirect: true });
        }
    }

    render() {
        var info;
        if (this.state.doesNotRedirect) {
            info = <Typography component="p" variant="body1">
                The application did not ask to redirect you back, you can close this
                window or tab.
            </Typography>;
        } else {
            info = <LinearProgress/>;
        }

        return (
            <AuthPage>
                <Head>
                    <title>Sapphirite Accounts | Log Out</title>
                </Head>
                <Grid item xs={12}>
                    <AuthPaper>
                        <Typography component="h1" variant="h5" gutterBottom>
                            You were logged out successfully
                        </Typography>
                        {info}
                    </AuthPaper>
                </Grid>
            </AuthPage>
        );
    }
};

export default LogoutPage;
