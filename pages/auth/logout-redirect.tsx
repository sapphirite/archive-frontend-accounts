import { NextContext } from "next";
import Head from "next/head";
import getConfig from "next/config";
import React from "react";
import { Grid, Typography, LinearProgress } from "@material-ui/core";

import AuthPage from "~/components/auth/AuthPage";
import AuthPaper from "~/components/auth/AuthPaper";

const config = getConfig().publicRuntimeConfig;

interface Props {
    redirectUri: string | null,
}

class LogoutRedirectPage extends React.Component<Props> {
    static async getInitialProps(context: NextContext): Promise<Partial<Props>> {
        if (context.query.redirect_uri) {
            return {
                redirectUri: context.query.redirect_uri as string,
            }
        } else {
            return {
                redirectUri: null,
            }
        }
    }

    componentDidMount() {
        // Make sure we have the latest redirect uri and no stale ones
        if (this.props.redirectUri) {
            window.localStorage.setItem("sa-accounts.logout-redirect", this.props.redirectUri);
        } else {
            window.localStorage.removeItem("sa-accounts.logout-redirect");
        }

        window.location.href = config.openidUrl + "/oauth2/auth/sessions/login/revoke";
    }

    render() {
        return (
            <AuthPage>
                <Head>
                    <title>Sapphirite Accounts | Log Out</title>
                </Head>
                <Grid item xs={12}>
                    <AuthPaper>
                        <Typography component="h1" variant="h5" gutterBottom>
                            Logging out...
                        </Typography>
                        <LinearProgress/>
                    </AuthPaper>
                </Grid>
            </AuthPage>
        );
    }
};

export default LogoutRedirectPage;
