import React from "react";
import { NextContext } from "next";
import Head from "next/head";
import Router from "next/router";
import { Grid } from "@material-ui/core";

import AuthPage from "~/components/auth/AuthPage";
import AppBadge from "~/components/AppBadge";
import ConsentForm from "~/components/auth/ConsentForm";
import AccountsService from "~/src/AccountsService";

interface Props {
    challenge: string,
    requestedScope: string[],
    clientName: string,
    clientOwner: string,
}

class ConsentPage extends React.Component<Props> {
    static async getInitialProps({query, res}: NextContext): Promise<Partial<Props>> {
        const challenge = query.consent_challenge as string;

        const accounts = new AccountsService();
        const result = await accounts.putConsentRequestStart(challenge);

        if (result.redirectTo) {
            if (res) {
                res.writeHead(302, { Location: result.redirectTo });
                res.end();
            } else {
                Router.push(result.redirectTo);
            }
            return {}
        }

        return {
            challenge: challenge,
            requestedScope: result.requestedScope,
            clientName: result.clientName,
            clientOwner: result.clientOwner,
        }
    }

    render() {
        return (
            <AuthPage>
                <Head>
                    <title>Sapphirite Accounts | Consent</title>
                </Head>
                <Grid item xs={12}>
                    <AppBadge
                        clientName={this.props.clientName}
                        clientOwner={this.props.clientOwner}
                    />
                </Grid>
                <Grid item xs={12}>
                    <ConsentForm
                        requestedScope={this.props.requestedScope}
                        challenge={this.props.challenge}
                    />
                </Grid>
            </AuthPage>
        )
    }
}

export default ConsentPage;