import React from "react"
import { NextContext } from "next";
import Head from "next/head";
import { Typography } from "@material-ui/core";
import { UserManager } from "oidc-client";

import PageMargins from "~/components/PageMargins";
import getUserManager from "~/src/getUserManager";
import Router from "next/router";

interface Props {
    error: boolean,
    title: string,
    description: string,
}

class CallbackPage extends React.Component<Props> {
    static async getInitialProps(context: NextContext): Promise<Partial<Props>> {
        // Display errors
        if (context.query.error) {
            return {
                error: true,
                title: context.query.error as string,
                description: context.query.error_description as string,
            }
        }

        return { }
    }

    async componentDidMount() {
        if (this.props.error) {
            return
        }

        const userManager = getUserManager() as UserManager;

        await userManager.signinRedirectCallback();
        Router.push("/");
    }

    render() {
        return (
            <PageMargins>
                <Head>
                    <title>Sapphirite Accounts</title>
                </Head>
                <Typography component="h1" variant="h4">{this.props.title}</Typography>
                <Typography component="p" variant="body1">{this.props.description}</Typography>
            </PageMargins>
        )
    }
};

export default CallbackPage;
