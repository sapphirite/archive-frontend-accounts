import React from "react";
import PropTypes from "prop-types";
import Document, { Head, Main, NextScript, NextDocumentContext, DefaultDocumentIProps } from "next/document";
import flush from "styled-jsx/server";
import { PageContext } from "../src/getPageContext";

class MyDocument extends Document {
    render() {
        return (
            <html lang="en" dir="ltr">
                <Head>
                    <meta charSet="utf-8" />
                    <meta
                        name="viewport"
                        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
                    />
                    <link rel="icon" type="image/x-icon" href="/static/favicon.ico" />
                    <link
                        rel="stylesheet"
                        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
                    />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }

    static getInitialProps(ctx: NextDocumentContext) {
        // Render app and page and get the context of the page with collected side effects
        let pageContext: PageContext | undefined;
        const page = ctx.renderPage(Component => {
            const WrappedComponent = (props: any) => {
                pageContext = props.pageContext;
                return <Component {...props} />;
            };
        
            WrappedComponent.propTypes = {
                pageContext: PropTypes.object.isRequired,
            };
        
            return WrappedComponent;
        });
      
        // It might be undefined, e.g. after an error
        let css;
        if (pageContext) {
            css = pageContext.sheetsRegistry.toString();
        } else {
            css = ""
        }
      
        return {
            ...page,
            pageContext,
            // Styles fragment is rendered after the app and page rendering finish
            styles: [(
                <React.Fragment key="0">
                    <style
                        id="jss-server-side"
                        dangerouslySetInnerHTML={{ __html: css }}
                    />
                    {flush() || null}
                </React.Fragment>
            )],
        } as DefaultDocumentIProps;
    };
}

export default MyDocument;
