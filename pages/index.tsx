import React from "react"
import Head from "next/head";
import { Typography, CssBaseline } from "@material-ui/core";
import { UserManager } from "oidc-client";

import getUserManager from "~/src/getUserManager";
import LoginButton from "~/components/LoginButton";
import PageMargins from "~/components/PageMargins";
import LogoutButton from "~/components/LogoutButton";

interface State {
    loadedUser: boolean,
    user: string | null,
}

class IndexPage extends React.Component<{}, State> {
    constructor(props: {}) {
        super(props);

        this.state = {
            loadedUser: false,
            user: null,
        };
    }

    async componentDidMount() {
        const userManager = getUserManager() as UserManager;

        const user = await userManager.getUser();
        if (user && !user.expired) {
            this.setState({ user: user.profile.sub });
        }

        this.setState({ loadedUser: true });
    }

    render() {
        var userOrLogin = <div></div>;
        if (this.state.loadedUser) {
            if (this.state.user) {
                userOrLogin = (
                    <div>
                        <Typography component="p" variant="body1" paragraph>
                            Hello, {this.state.user}!
                        </Typography>
                        <LogoutButton/>
                    </div>
                );
            } else {
                userOrLogin = (
                    <div>
                        <Typography component="p" variant="body1" paragraph>
                            Log in to manage your account.
                        </Typography>
                        <LoginButton/>
                    </div>
                );
            }
        }

        return (
            <PageMargins>
                <Head>
                    <title>Sapphirite Accounts</title>
                </Head>
                <Typography component="h1" variant="h4">Sapphirite Accounts</Typography>
                {userOrLogin}
                <CssBaseline/>
            </PageMargins>
        )
    }
};

export default IndexPage;
