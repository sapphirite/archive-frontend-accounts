import { SheetsRegistry, GenerateClassName } from "jss";
import { createMuiTheme, createGenerateClassName, Theme } from "@material-ui/core/styles";

const theme = createMuiTheme({
    palette: {
        type: "dark",
        background: {
            default: "#343232",
        },
    },
    typography: {
        useNextVariants: true,
    },
});

export interface PageContext {
    theme: Theme,
    sheetsManager: Map<any, any>,
    sheetsRegistry: SheetsRegistry,
    generateClassName: GenerateClassName<any>,
}

function createPageContext(): PageContext {
    return {
        theme,
        // This is needed in order to deduplicate the injection of CSS in the page.
        sheetsManager: new Map(),
        // This is needed in order to inject the critical CSS.
        sheetsRegistry: new SheetsRegistry(),
        // The standard class name generator.
        generateClassName: createGenerateClassName(),
    };
}

let pageContext: PageContext;

export default function getPageContext() {
    // Make sure to create a new context for every server-side request so that data
    // isn"t shared between connections (which would be bad).
    if (!(process as any).browser) {
        return createPageContext();
    }

    // Reuse context on the client-side.
    if (!pageContext) {
        pageContext = createPageContext();
    }

    return pageContext;
}
