//! Global user manager, only available on the client

import getConfig from "next/config"

type UserManager = import("oidc-client").UserManager;

const config = getConfig().publicRuntimeConfig;

var userManager: UserManager | null = null;

export default function getUserManager() {
    // If we've already got one return it
    if (userManager) {
        return userManager;
    }

    // We don't have one, if we're not on the browser return null
    if (!(process as any).browser) {
        return null;
    }

    // Code gets run on import that breaks on the server, so we need to only import it once we're
    // sure we're on the client and need to create the UserManager.
    var Oidc = require("oidc-client");

    // Enable for debugging
    //Oidc.Log.logger = console;
    //Oidc.Log.level = Oidc.Log.DEBUG;

    var settings = {
        authority: config.openidUrl,
        client_id: "devclient",
        redirect_uri: window.location.origin + "/callback",
        //post_logout_redirect_uri: url + "/code-identityserver-sample.html",
        response_type: "code",
        scope: "openid",

        //popup_redirect_uri: url + "/code-identityserver-sample-popup-signin.html",
        //popup_post_logout_redirect_uri: url + "/code-identityserver-sample-popup-signout.html",

        //silent_redirect_uri: url + "/code-identityserver-sample-silent.html",
        automaticSilentRenew:true,

        filterProtocolClaims: true,
        loadUserInfo: false,
        revokeAccessTokenOnSignout : true
    };
    userManager = new Oidc.UserManager(settings);

    return userManager;
}