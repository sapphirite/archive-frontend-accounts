import Axios, { AxiosInstance } from "axios";
import getConfig from "next/config"

const config = getConfig().publicRuntimeConfig;

export default class AccountsService {
    axios: AxiosInstance;

    constructor() {
        var url: string;
        if ((process as any).browser) {
            url = config.clientApiAccountsUrl;
        } else {
            url = config.serverApiAccountsUrl;
        }

        this.axios = Axios.create({
            baseURL: url,
            validateStatus: status => status >= 200 && status <= 400,
        });
    }

    async putConsentRequestStart(
        challenge: string,
    ): Promise<CompletedConsentRequestStart> {
        const response = await this.axios.put(
            "/consent/request/" + challenge + "/start"
        );

        return response.data;
    }

    async putConsentRequestAccept(
        challenge: string,
    ): Promise<CompletedConsentRequestAction> {
        const response = await this.axios.put(
            "/consent/request/" + challenge + "/accept"
        );

        return response.data;
    }
    
    async putConsentRequestReject(
        challenge: string,
    ): Promise<CompletedConsentRequestAction> {
        const response = await this.axios.put(
            "/consent/request/" + challenge + "/reject"
        );

        return response.data;
    }

    async putLoginRequestStart(
        challenge: string,
    ): Promise<CompletedLoginRequestStart> {
        const response = await this.axios.put(
            "/login/request/" + challenge + "/start"
        );

        return response.data;
    }

    async putLoginRequestAttempt(
        challenge: string, attempt: LoginRequestAttempt,
    ): Promise<CompletedLoginRequestAttempt | ApiAccountsError> {
        const response = await this.axios.put(
            "/login/request/" + challenge + "/attempt",
            attempt,
        );

        if (response.status === 200 || response.status === 400) {
            return response.data;
        }

        throw this.badStatus(response.status);
    }

    private badStatus(status: number): Error {
        return new Error(`Unexpected status code ${status} from API`);
    }
}

export interface CompletedConsentRequestStart {
    redirectTo?: string,
    requestedScope?: string[],
    clientName?: string,
    clientOwner?: string,
}

export interface CompletedConsentRequestAction {
    redirectTo?: string,
}

export interface CompletedLoginRequestStart {
    redirectTo?: string,
    clientName?: string,
    clientOwner?: string,
}

export interface LoginRequestAttempt {
    email: string,
    password: string,
    remember: boolean,
}

export interface CompletedLoginRequestAttempt {
    readonly redirectTo: string,
}

export interface ApiAccountsError {
    readonly error: string,
}

export function isApiAccountsError(obj: any): obj is ApiAccountsError {
    return 'error' in obj;
}
